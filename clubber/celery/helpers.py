import os
import urllib.request
import urllib.parse
import subprocess


SERVICE_CONNNECTED = False
try:
    from service.service import Service
    SERVICE_CONNNECTED = True
except ImportError as error:
    print(f'Error while trying to import Service: {error}')


def run_command(command, shell=False, print_output=True, env_exports={}):
    current_env = os.environ.copy()
    merged_env = {**current_env, **env_exports}
    process = subprocess.Popen(command, shell=shell, env=merged_env, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout = []
    stdout_data, stderr_data = process.communicate()
    for line in stdout_data.splitlines():
        line = line.rstrip().decode('utf8')
        if print_output:
            print(f'shell> {line}')
        stdout.append(line)
    if process.returncode != 0:
        stderr = []
        stderr_data = '' if not stderr_data else stderr_data
        for line in stderr_data.splitlines():
            line = line.rstrip().decode('utf8')
            stderr.append(line)
        print(f'Error while executing command: {" ".join(stderr)}')
    return stdout

def webhook_callback(url_api):
    try:
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for callback: {url_api} [{err}]')

def import_service():
    return Service if SERVICE_CONNNECTED else None
