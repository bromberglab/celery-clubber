import os
import shutil
from datetime import datetime
from pathlib import Path
from .helpers import run_command, webhook_callback, import_service
from .worker import app


HOSTNAME = os.getenv('HOSTNAME', 'clubber')
APP_PATH = Path(os.getenv('APP_PATH', '/app'))
APP_SCRATCH = Path(os.getenv('APP_SCRATCH', '/scratch')) / 'clubber'
KEEP_APP_SCRATCH_DAYS = int(os.getenv('KEEP_APP_SCRATCH_DAYS', 7))


@app.task(bind=True, name='cron', queue='beat')
def cron(self, tasks):  
    for task in tasks:
        print(f'got task: {task}')
        if task == 'cleanup':
            for sfolder in APP_SCRATCH.glob('*'):
                createTime = datetime.fromtimestamp(sfolder.stat().st_atime)
                delta = datetime.now() - createTime
                if delta.days > KEEP_APP_SCRATCH_DAYS:
                    shutil.rmtree(sfolder)
        elif task == 'beat':
            print(f'Got beat: {HOSTNAME}')


@app.task(bind=True, name='clubber.submit')  
def submit(self, *args, **kwargs):

    # process taks
    print(f'New local submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs.get('submission_id', 'null')

    Service = import_service()
    if not Service:
        print(f'[Exception] Failed to import service - no service was executed.')
    else:
        args = kwargs.pop('args_', args)
        action = kwargs.pop('action', 'default')

        service = Service(*args,**kwargs)
        service.run(action)

        # deprecated: call service via run.sh
        # cmd = ['/app/service/run.sh']
        # for key, val in kwargs.items():
        #     cmd.extend([key, str(val)])
        # run_command(cmd)

        status = "ok"

        webhook_callback(url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}')
        return (sid, status)


@app.task(bind=True, name='clubber.status')  
def status(self, *args, **kwargs):

    # process taks
    print(f'New local submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs.get('submission_id', 'null')

    Service = import_service()
    if not Service:
        print(f'[Exception] Failed to import service - no service was executed.')
    else:
        args = kwargs.pop('args_', args)
        action = kwargs.pop('action', 'default')

        service = Service(*args,**kwargs)
        result = service.run(action)

        status = "ok"

        webhook_callback(url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}')
        return (sid, status, result)
