import os

# Broker settings.
broker_url = os.environ.get('CELERY_BROKER_URL', 'redis://')

# Result backend setting.
result_backend = os.environ.get('CELERY_RESULT_BACKEND', 'redis://')

# Task settings.
task_default_queue = 'clubber'
